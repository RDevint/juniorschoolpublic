function blend(img)
{
    //alert("Try blend");
    var mode = img.attr("blend-mode");
    //alert(mode);
    var color = img.attr("blend-color");
    // alert(color);
    //img.css("visibility","hidden");
    var src = img.attr("src");
    img.attr("src","../images/transparent.png");
    img.addClass(mode);
    img.css("background-image",'url('+src+')');
    img.css("background-size",'100% 100%');
    img.css("background-color",color);

}

$(document).ready(function(){
    blend($("img.blend"));
});