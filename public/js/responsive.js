var modes = ['strict','student','school','junior'];
var current = 0;

$(document).ready(function () {
    $("#viewmode").click(function(){
        var lastMode = current%modes.length;
        current++;
        var newMode = current%modes.length;
        $("body").removeClass(modes[lastMode]);
        $("body").addClass(modes[newMode]);
    });
});