<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username'=>'admin',
            'email'=>'admin@mail.ru',
            'password'=>bcrypt('admin'),
            'phone'=>'8800555',
            'first_name'=>'Ivan',
            'last_name'=>'Pupkin',
            'birth_date'=>\Carbon\Carbon::parse('2000-01-01'),
            'type'=>'admin'
        ]);
    }
}
