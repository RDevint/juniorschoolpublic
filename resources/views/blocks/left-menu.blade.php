<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10.11.2017
 * Time: 6:12
 */
?>

<div class="glass">
    <div class="sidebar-wrapper">
    <nav id="sidebar">
        <!-- Sidebar Header -->
        <div class="sidebar-header">
            <div class="sidebar-description">Навигация по сайту</div>
        </div>
        <!-- Sidebar Links -->
        <ul class="list-unstyled components">
            <li>
                <a href="#"><img src="../public/images/cups_logo.png" alt="Engineering_school" width="30" height="30">  Олимпиады</a>
            </li>
            <li>
                <a href="#"><img src="../public/images/contests_logo.png" alt="Engineering_school" width="30" height="30">  Конкурсы</a>
            </li>
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"><img src="../public/images/projects_logo.png" alt="Engineering_school" width="30" height="30">  Проекты</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li><a href="#"><img src="../public/images/robots_logo.png" alt="Engineering_school" width="30" height="30">  Робототехника</a></li>
                    <li><a href="#"><img src="../public/images/progs_logo.png" alt="Engineering_school" width="30" height="30">  Програмное обеспечение</a></li>
                    <li><a href="#"><img src="../public/images/games_logo.png" alt="Engineering_school" width="30" height="30">  Игры</a></li>
                </ul>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"><img src="../public/images/events_logo.png" alt="Engineering_school" width="30" height="30">  События</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li><a href="#">Page 1</a></li>
                    <li><a href="#">Page 2</a></li>
                    <li><a href="#">Page 3</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="../public/images/media_logo.png" alt="Engineering_school" width="30" height="22.5">  Медиа</a>
            </li>
            <li>
                <a href="#"><img src="../public/images/info_logo.png" alt="Engineering_school" width="30" height="30">  Информация</a>
            </li>
        </ul>
    </nav>
    </div>

</div>
