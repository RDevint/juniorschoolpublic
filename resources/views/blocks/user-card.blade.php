<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10.11.2017
 * Time: 15:48
 */
?>
<!--User mini menu-->
<div id="umenu" class="umenu">
    <div class="uarea">
        <img src="images/user.png" alt="" class="img-thumbnail">
        <div class="uname">
            @if(Auth::guest())
                <a href="#" align="center">Гость</a>
            @else
                <a href="#" align="center">{{ Auth::user()->username }}</a>
            @endif
        </div>

        <!--Buttons-->
        @if(Auth::guest())
            <ul class="buttons">
                <li><a class="btn btn-default" href="{{ route('login') }}">Login</a> </li>
                <li class="shdhelper"></li>
                <li><a class="btn btn-default" href="{{ route('register') }}">Register</a></li>
            </ul>
        @else
            <ul class="buttons">
                <li><img src="../public/images/gly_user.png" align="middle" width="11" class="btn btn-default"></li>
                <li><img src="../public/images/gly_message.png" align="middle" width=11" class="btn btn-default"></li>
                <li><img src="../public/images/gly_exit.png" align="middle" width="11" class="btn btn-default"><a href="logout"></a></li>
                <li class="shdhelper"></li>
            </ul>>
    </div>
    @endif
</div>

