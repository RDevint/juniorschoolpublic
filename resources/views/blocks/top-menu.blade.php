<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10.11.2017
 * Time: 6:11
 */
?>

<nav id="navbar" class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="/">
                <img src="../public/images/navbar_logo.png" alt="Engineering_school" width="200" height="50">
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Домой</a></li>
                <li><a href="#">Проекты</a></li>
                <li><a href="#">Информация</a></li>
            </ul>
            <form id="search" method="POST" action="/search">
                <input id="searchInput" type="text" name="search" placeholder="Поиск">
                <button id="searchBtn" type="submit" value="Поиск">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </form>
            <!--User-->
            @include('blocks.user-card')
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>
