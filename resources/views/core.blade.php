<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10.11.2017
 * Time: 6:10
 */
?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{config('app.name')}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../css/filters.css" rel="stylesheet" type="text/css">
    <link href="../css/app.css" rel="stylesheet" type="text/css">
    <link href="../css/themes.css" rel="stylesheet" type="text/css">



    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery-sortable.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/responsive.js"></script>
    <script type="text/javascript" src="js/blend.js"></script>


</head>
<body>
<a class="btn btn-default" id="viewmode">Перекрасить</a>
@if(isset($layout))
    @include("layouts.{$layout}")
@else
    @include("layouts.default")
@endif
</body>
</html>