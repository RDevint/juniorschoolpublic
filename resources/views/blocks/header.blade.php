<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10.11.2017
 * Time: 6:11
 */
?>


<div id="header" class="glass">
    <div class="site-description">Портал поддержки научно-технического и инженерного творчества детей и подростков Волгоградской области</div>
    <div class="header-inner">
        <div class="col-md-6 site-logo">&nbsp;
            <a class="btn btn-info connect"><span class="glyphicon glyphicon-education"></span> Присоединиться!</a>
        </div>
        <div class="col-md-2 site-summary" >
            Нас уже 0 человек<br>
            0 проектов<br>
            0 заявок
        </div>
        <div class="col-md-4 vstu-logo">
            <img src="images/VSTU.png" align="center"/>
        </div>
    </div>
</div>


