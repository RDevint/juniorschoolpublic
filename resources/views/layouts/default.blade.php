<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10.11.2017
 * Time: 6:10
 */
?>
<!-- NAVBAR -->
@include("blocks.top-menu")
<!-- END NAVBAR -->

<!-- MESSAGE -->
@include("blocks.message")
<!-- END MESSAGE -->

<!-- HEADER -->
<div class="container">
    @include("blocks.header")
</div>
<!-- END HEADER -->


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <!-- LEFT MENU -->
        @include("blocks.left-menu")
        <!-- END LEFT MENU -->
        </div>
        <div class="col-md-9">
            <!-- CONTENT -->
        @include("blocks.content")
        <!-- END CONTENT -->
        </div>
    </div>
</div>
<!-- ADVERT -->
<div class="container">
    @include("blocks.advert")
</div>
<!-- END ADVERT -->

<!-- FOOTER -->
<div class="container">
    @include("blocks.footer")
</div>
<!-- END FOOTER -->

<!-- MODAL -->
@include("blocks.modal")
<!-- END MODAL -->
